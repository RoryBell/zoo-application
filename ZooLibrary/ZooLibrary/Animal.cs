﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZooLibrary
{
    public enum Status
    {
        Alive,
        Unstable,
        Dead
    }

    /// <summary>
    /// Base class for animals.
    /// </summary>
    public abstract class Animal
    {
        /// <summary>
        /// Increases the health by a value randomly generated between 10 and 25.
        /// </summary>
        public void Feed()
        {
            // Generate a value between 10 and 25.
            Random rand = new Random();
            double value = rand.NextDouble() * 15f + 10f;

            // Increase the health by the randomly generated value.
            m_health += value;
        }

        /// <summary>
        /// Reduces the health by a value randomly generated between 0 and 20.
        /// </summary>
        public void Damage()
        {
            // Generate a value between 0 and 20.
            Random rand = new Random();
            double value = rand.NextDouble() * 20f;

            // Decrease the health by the randomly generated value.
            m_health -= value;
        }

        public abstract Status Status { get; }

        /// <summary>
        /// The health of the animal in percentage.
        /// </summary>
        private double m_health = 100f;
    }
}

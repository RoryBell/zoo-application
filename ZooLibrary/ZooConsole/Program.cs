﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZooLibrary;

namespace ZooConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animalList = new List<Animal>();

            animalList.Add(new Monkey());
            animalList.Add(new Elephant());
            animalList.Add(new Giraffe());

            foreach (Animal animal in animalList)
            {
                Console.WriteLine($"Animal is type {animal}");
            }
        }
    }
}
